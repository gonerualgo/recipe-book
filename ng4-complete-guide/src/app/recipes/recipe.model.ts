export class Recipe {
    public name: string;
    public description: string;
    public imagePath: string;

    //Built in function every class has that will be executed once you create a new instance to this class
    constructor(name: string, desc: string, imagePath: string){
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;

    }
}